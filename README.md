# BINAR FSW5 Challenge-07 Kelompok 4  
 
## Anggota Kelompok  
1. Audima Oktasena 
2. Robiata Tsania Salsabila  
3. Cahyo Tri Nugroho 
 
## Running Frontend
```sh
npm start
```

## Running Server Backend
```sh
node server.js
```

## Endpoint 
http://localhost:3000/      : Landing page <br />
http://localhost:3000/login : Login using OAuth Google <br />
http://localhost:3000/cars  : Cars page for get all data and filtering


