import React, { Component, useState, useEffect } from "react";
import axios from "axios";
import { Row } from "react-bootstrap";
// import "./CariMobil.css";
import fi_users from '../../../assets/fi_calendar.png'
import fi_settings from '../../../assets/fi_settings.png'
import fi_calendar from '../../../assets/fi_users.png'

const CarContent3 = (props) => {
  const [posts, setPosts] = useState({ blogs: [] });

  useEffect(() => {
    const fetchPostsList = async () => {
      const { data } = await axios(
        "https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json"
      );

      setPosts({ blogs: data });
      console.log(data);
    };
    fetchPostsList();
  }, [setPosts]);
  return (
    <div className="container">
        <Row>
            <section className="cars  d-flex justify-content-center mt-5">
                <div className="insert-card-cars row px-4">
                    {posts.blogs &&
                        posts.blogs.map((item) => (
                            <div className="col-lg-4 col-md-6 col-sm-12 mb-4">
                                <div className="card-cars">
                                    <img className="cars-img " src={item.image} alt="" />
                                    <div className="title-cars">
                                        <h5>{item.manufacture}/{item.model}</h5>
                                        <h3>Rp {item.rentPerDay} / hari</h3>
                                        <p>{item.description}</p>
                                    </div>
                                    <div className="spec-cars-detail  ">
                                        <div className="spec-detail d-flex justify-content-start">
                                            <img className="spec-img" src={fi_users} alt="" />
                                            <p className="align-self-center mb-0">{item.capacity} orang</p>
                                        </div>
                                        <div className="spec-detail d-flex justify-content-start">
                                            <img className="spec-img" src={fi_settings} alt="" />
                                            <p className="align-self-center mb-0">{item.transmission}</p>
                                        </div>
                                        <div className="spec-detail d-flex justify-content-start">
                                            <img className="spec-img" src={fi_calendar} alt="" />
                                            <p className="align-self-center mb-0">Tahun {item.year}</p>
                                        </div>
                                    </div>
                                    <div className="choose-cars">
                                        <button className="btn btn-choose-cars">
                                            <a href="#">Pilih Mobil</a>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}
                </div>
            </section>
            
        </Row>
    </div>
  );
};

export default CarContent3;
