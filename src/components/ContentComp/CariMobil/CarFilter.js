import "./CariMobil.css";
import { Row } from "react-bootstrap";
import fi_users from '../../../assets/fi_calendar.png'
import fi_settings from '../../../assets/fi_settings.png'
import fi_calendar from '../../../assets/fi_users.png'

const CarFilter = ({ cars }) => {
  return (
    <div className="container justify-content-center">
      <Row>
          <section className="cars  d-flex justify-content-center mt-5">
            <div className="insert-card-cars row px-4">
            {cars.map((car) => (
                <div className="col-lg-4 col-md-6 col-sm-12 mb-4">
                    <div className="card-cars">
                        <img className="cars-img " src={car.image} alt="" />
                        <div className="title-cars">
                            <h5>{car.manufacture}/{car.model}</h5>
                            <h3>Rp {car.rentPerDay} / hari</h3>
                            <p>{car.description}</p>
                        </div>
                        <div className="spec-cars-detail  ">
                            <div className="spec-detail d-flex justify-content-start">
                                <img className="spec-img" src={fi_users} alt="" />
                                <p className="align-self-center mb-0">{car.capacity} orang</p>
                            </div>
                            <div className="spec-detail d-flex justify-content-start">
                                <img className="spec-img" src={fi_settings} alt="" />
                                <p className="align-self-center mb-0">{car.transmission}</p>
                            </div>
                            <div className="spec-detail d-flex justify-content-start">
                                <img className="spec-img" src={fi_calendar} alt="" />
                                <p className="align-self-center mb-0">Tahun {car.year}</p>
                            </div>
                        </div>
                        <div className="choose-cars">
                            <button className="btn btn-choose-cars">
                                <a href="#">Pilih Mobil</a>
                            </button>
                        </div>
                    </div>
                </div>    
                ))} 
            </div>
          </section>
      </Row>
    </div>
  );
};

export default CarFilter;
