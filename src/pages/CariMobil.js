import React, { useEffect } from "react";
import { CarContent1 } from "../components/ContentComp/CariMobil/CarContent1";
import { CarContent2 } from "../components/ContentComp/CariMobil/CarContent2";
import { NavbarComp } from "../components/NavbarComp/NavbarComp";

export const CariMobil = () => {
  // useEffect(() => {
  //   document.title = "Search Cars";
  // }, []);

  return (
    <div>
      <NavbarComp />
      <CarContent1 />
      <CarContent2 />
    </div>
  );
};
