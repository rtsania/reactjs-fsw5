import { gapi } from "gapi-script";
import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import GoogleLogin from "react-google-login";
import { Navigate } from "react-router-dom";
import { FooterComp } from "../components/FooterComp/FooterComp";
import { NavbarComp } from "../components/NavbarComp/NavbarComp";
import { CariMobil } from "./CariMobil";
import "./LoginPage.css";

export const LoginPage = () => {
  const [loginData, setLoginData] = useState(
    localStorage.getItem("loginData")
      ? JSON.parse(localStorage.getItem("loginData"))
      : null
  );

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID,
      });
    }

    gapi.load("client:auth2", start);
  });

  const handleLogin = async (googleData) => {
    const res = await fetch("/api/google-login", {
      method: "POST",
      body: JSON.stringify({
        token: googleData.tokenId,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    setLoginData(data);
    localStorage.setItem("loginData", JSON.stringify(data));
  };

  const handleFailure = (result) => {
    alert("Login Gagal");
  };

  const handleLogout = () => {
    localStorage.removeItem("loginData");
    setLoginData(null);
  };

  return (
    <div>
      {loginData ? (
        (console.log(loginData),
        (
          <div>
            <Navigate to={"/cars"} />
            <FooterComp />
          </div>
        ))
      ) : (
        <div>
          <NavbarComp
            handleLogout={handleLogin}
            handleFailure={handleFailure}
            loginData={loginData}
          />
          <Container className="LoginPage">
            <div className="text">
              <h2 className="fw-bold">Sign in </h2>
              <p>Melakukan login melalui google account </p>
              <GoogleLogin
                clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                buttonText="Login with Google"
                onSuccess={handleLogin}
                onFailure={handleFailure}
                cookiePolicy={"single_host_origin"}
                isSignedIn={true}
              ></GoogleLogin>
            </div>
          </Container>
        </div>
      )}
    </div>
  );
};
